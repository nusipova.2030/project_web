import { SearchInputService } from './../search-input/search-input.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  footerBlocks = [
    {
      title: 'Company',
      items: [
        {
          link: 'About Us'
        },
        {
          link: 'Dermstore Spa'
        },
        {
          link: 'Testimonials'
        },
        {
          link: 'Press'
        },
        {
          link: 'Brand Submissions'
        },
        {
          link: 'eGift Cards'
        },
      ]
    },
    {
      title: 'Customer Service',
      items: [
        {
          link: 'Customer Service'
        },
        {
          link: 'FAQs'
        },
        {
          link: 'About Rewards'
        },
        {
          link: 'Order Status'
        },
        {
          link: 'Shipping Information'
        },
        {
          link: 'Product Recall'
        },
      ]
    },
    {
      title: 'My Account',
      items: [
        {
          link: 'Dermstore Subscriptions'
        },
        {
          link: 'My Rewards'
        },
        {
          link: 'My Favorites'
        },
        {
          link: 'Order History'
        },
        {
          link: 'Order History'
        },
      ]
    }
  ];

  placeholder = "Email Address"
  iconSearch = "fa fa-angle-right"
  form?: FormGroup;
  constructor(
    private searchService: SearchInputService
  ) { }

  ngOnInit(): void {
    this.form = this.searchService.createSearchForm();
  }

}
