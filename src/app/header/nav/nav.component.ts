import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
   navItems = [
     {
       title: 'Browse By'
     },
     {
      title: 'Brands'
     },
     {
      title: 'Skin Care'
     },
     {
      title: 'Makeup'
     },
     {
      title: 'Haircare'
     },
     {
      title: 'Bath & Body'
     },
     {
      title: 'Men'
     },
     {
      title: 'Gift & Sets'
     },
     {
      title: 'Sale'
     },
     {
      title: 'Skin 101'
     },
   ];
  constructor() { }

  ngOnInit(): void {
    console.log(this.navItems);
  }

}
