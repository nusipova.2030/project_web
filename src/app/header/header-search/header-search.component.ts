import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { SearchInputService } from 'src/app/search-input/search-input.service';

@Component({
  selector: 'app-header-search',
  templateUrl: './header-search.component.html',
  styleUrls: ['./header-search.component.scss']
})
export class HeaderSearchComponent implements OnInit {
 placeholder = "SEARCH DERMSTORE";
 iconSearch = "fa fa-search";

 form: FormGroup | undefined;

  constructor(
    private searchService: SearchInputService
  ) { }

  ngOnInit(): void {
    this.form = this.searchService.createSearchForm()
  }

}
