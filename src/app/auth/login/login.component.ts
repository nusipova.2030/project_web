import { User } from './../../libs/api-interfaces';
import { AuthService } from './../auth.service';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  errorMessage: string;
  pageTitle = 'Log In';
  loginForm: NgForm;
  empty = false;

  constructor(private authService: AuthService,
              private router: Router) { }
  ngOnInit(): void {
  }

  canExit(): boolean{
    if (confirm("Do you wish to Please confirm")) {
      return true
    } else {
      return false
    }
  }


  login(loginForm: NgForm) {
    if (loginForm && loginForm.valid) {
      this.empty=true;
      const userName = loginForm.form.value.userName;
      const password = loginForm.form.value.password;
      this.authService.login(userName, password);

      if (this.authService.redirectUrl) {
        this.router.navigateByUrl(this.authService.redirectUrl);
      } else {
        this.router.navigate(['/profile']);
      }
    } else {
      this.errorMessage = 'Please enter a user name and password.';
    }
  }



}
