import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { User } from '../libs/api-interfaces';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  users: User[];
  redirectUrl: string;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: User;

  constructor(private http: HttpClient) {
    this.getUsers().subscribe((users) => (this.users = users));
  }
  get isLoggedIn(): boolean {
    return !!this.currentUser;
  }
  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`http://localhost:3000/users`);
  }
  login(userName: string, password: string): void {
    if (!userName || !password) {
      console.log('nono');
      return;
    }
    for (const u of this.users) {
      if (userName === u.login && password === u.passwd) {
        this.currentUser = {
          login: userName,
          passwd: password,
          token: 'fake_token',
        };
        //window.location.reload();
        this.redirectUrl = '/profile';
        localStorage.setItem('current', JSON.stringify(this.currentUser));

        //localStorage.setItem('currentUser', this.currentUser.token);
        console.log(localStorage.getItem('current'));
        return;
      }
      // else {
      //   alert('Wrong password or username');
      // }
    }
  }

  register(userName: string, password: string): void {
    console.log(this.users);
    if (!userName || !password) {
      console.log('nono');
      return;
    }
    for (const u of this.users) {
      if (userName === u.login){
        alert('User exists');
      }
      else{
        const user = { login: userName, passwd: password };
        console.log('Register');
        this.http
          .post<User>('http://localhost:3000/users', user)
          .subscribe((res) => {
            console.log(res);
          });
          this.redirectUrl = '/register';
        console.log(user);

        return;
      } 
    }
  }

  logout(): void {
    localStorage.removeItem('current');
    window.location.reload();
  }
}
