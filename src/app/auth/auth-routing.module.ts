import { RegisterComponent } from './register/register.component';
import { EditGuard } from './edit.guard';
import { LoginComponent } from './login/login.component';
import { Route, RouterModule } from '@angular/router';
import { NgModule } from "@angular/core";

const routes: Route[] = [
  {
    path: '',
    component: LoginComponent, canDeactivate:[EditGuard]
  },
  { path: 'register', component: RegisterComponent}

];
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class
AuthRoutingModule {
}
