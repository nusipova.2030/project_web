import { Injectable } from '@angular/core';
import { SearchInputComponent } from './search-input.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Injectable({providedIn: 'root'})
export class SearchInputService {

  constructor(

  ){}
  ngOnInit(){
    this.createSearchForm();
  }
  createSearchForm(){
    const form = new FormGroup({
      search: new FormControl('', [Validators.required, Validators.minLength(3)])

    });
    return form;
  }

}
