import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
 articleItems = [

   {
    src: 'https://www.dermstore.com/blog/wp-content/uploads/2019/11/white-cream-texture-feature-1.jpg',
    title: 'Resveratrol 101: What You Need to Know About This Anti-Aging MVP',
    author: 'Janeca Racho',
    data: '2021-03-11T14:28:51.000Z'
  },
  {
    src: 'https://www.dermstore.com/blog/wp-content/uploads/2020/11/ddg-blog-featured-3-21.jpg',
    title: '7 Best Beauty Devices to Help Solve Your Skin Care Concerns',
    author: 'Amanda Mitchell',
    data: '2021-03-10T14:28:51.000Z'
  },
  {
    src: 'https://www.dermstore.com/blog/wp-content/uploads/2021/01/skin-care-products-feature.jpg',
    title: 'The Only 3 Steps Your Skin Care Routine Needs',
    author: 'Wendy Rodewald-Sulz',
    data: '2021-01-14T14:28:51.000Z'
  },
  {
    src: 'https://www.dermstore.com/blog/wp-content/uploads/2021/03/woman-touching-her-arm-feature.jpg',
    title: 'How to Get Rid of Keratosis Pilaris, According to a Dermatologist',
    author: 'Janeca Racho',
    data: '2021-03-09T14:28:51.000Z'
  },
  {
    src: 'https://www.dermstore.com/blog/wp-content/uploads/2021/02/woman-applying-skincare-feature.jpg',
    title: '6 Things to Know Before Using Hydroxy Acids on Your Skin',
    author: 'Rebecca R. Norris',
    data: '2021-03-11T14:28:51.000Z'
  },
 ]

  constructor() { }

  ngOnInit(): void {
  }

}
