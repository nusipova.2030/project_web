import { Observable } from 'rxjs';
import { RetrieveService } from './../retrieve.service';
import { FormsModule, FormControl, Validators } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  placeholder = 'Search Blog';
  email_input = new FormControl('',[Validators.required, Validators.email]);
  @Output() onClick = new EventEmitter();
  navItems = [
    {
      title: "Beginner's guide",
      link: '/beginner'
    },
    {
      title: "the doctor's office",
      link: '/next'

    },
    {
      title: "skin care",
      link: '/beginner'
    },
    {
      title: "anti aging",
      link: '/next'
    },
    {
      title: "top product picks",
      link: '/beginner'
    },
    {
      title: "Dermatologist Reviewed",
      link: '/next'
    },
    {
      title: "how-to",
      link: '/beginner'
    },
    {
      title: "makeup",
      link: '/next'
    },
    {
      title: "hair care",
      link: '/beginner'
    },
    {
      title: "bath & body",
      link: '/next'
    },
    {
      title: "natural",
      link: '/beginner'
    },
    {
      title: "lifestyle",
      link: '/next'
    },
    {
      title: "dermstore news",
      link: '/beginner'
    },
    {
      title: "videos",
      link: '/next'
    },

  ]
 //navItems$: Observable<any>;
  constructor(
    private retrieveService: RetrieveService
  ) { }

  ngOnInit(): void {
    //console.log(this.navItems$);
   // this.getNav();
  }
  send() {
    alert("Sent to "+ this.email_input.value);
  }
  // getNav(){
  //   this.navItems$ = this.retrieveService.getNav();
  // }

}
