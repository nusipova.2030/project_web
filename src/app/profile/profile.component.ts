import { User } from './../libs/api-interfaces';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
   current_user:User = JSON.parse(localStorage.getItem('current') || '{}');
  constructor() {

   }



  ngOnInit(): void {
    
  }

}
