export interface User {
  id?: number;
 login: string;
 passwd?: string;
 token: string;
}
